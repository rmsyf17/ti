@extends('layout.web.app')
@section('title','Home')

@section('content')
<!-- begin:content -->
<div class="col-md-12 col-sm-8 content">
	<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12 col-sm-6">
				<h3>Create An Account</h3>
				<hr />
				<form action="{{ url('postregistrasi') }}" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="email">Name</label>
					<input type="text" name="name" value="{{old('name')}}" class="form-control" >
					@if($errors->has('name'))
						<div class="text-danger">
						{{ $errors->first('name')}}
						</div>
					@endif
				</div>
				<div class="form-group">
					<label for="email">E-mail</label>
					<input type="email" name="email" value="{{old('email')}}" class="form-control">
					@if($errors->has('email'))
						<div class="text-danger">
						{{ $errors->first('email')}}
						</div>
					@endif
				</div>
				<div class="form-group">
					<label for="email">Phonenumber</label>
					<input type="number" min="0" name="no_hp" value="{{old('no_hp')}}" class="form-control">
					@if($errors->has('no_hp'))
						<div class="text-danger">
						{{ $errors->first('no_hp')}}
						</div>
					@endif
				</div>
				<div class="form-group">
					<label for="email">Password</label>
					<input type="password" name="password" class="form-control">
					@if($errors->has('password'))
						<div class="text-danger">
						{{ $errors->first('password')}}
						</div>
					@endif
				</div>
				<div class="form-group">
					<label for="email">Address</label>
					<textarea class="form-control" name="alamat">{{old('alamat')}}</textarea>
					@if($errors->has('alamat'))
						<div class="text-danger">
						{{ $errors->first('alamat')}}
						</div>
					@endif
				</div>
				<!-- <a href="account.html" class="btn btn-primary">Register</a> -->
				<input type="submit" class="btn btn-primary" value="Register">
				</form>
			</div>
		</div>
	</div>
	</div>
</div>
@endsection
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Keranjang;
use App\Models\Order;
use App\Models\Transaksi;
use App\Models\Bank;
use App\User;
use App\Models\Pengiriman;
use App\Models\Province;
use App\Models\City;
use App\Models\Courier;
use Kavist\RajaOngkir\Facades\RajaOngkir;



class HomeController extends Controller
{
    //
    public function index()
    {
		$dataBarang = Product::where('status_aktif', 1)->orderBy('created_at', 'desc')->paginate(8);
		
		return view('web.home.index')->with(compact('dataBarang'));

		
	}

	public function detail($id)
    {
		$dataBarang = Product::where('id', $id)->first();
		return view('web.detail.index')->with(compact('dataBarang'));
	}

	public function cart(Request $request)
	{

		if ( Auth::check() ) {
			$dataKeranjang = Keranjang::where('user_id', Auth::user()->id)->get();
			if(!$dataKeranjang){
				$countKeranjang = 0;
				$dataSum = 0;
			}else{
				$countKeranjang = $dataKeranjang->count();
				$dataSum = Keranjang::where('user_id', Auth::user()->id)->sum('total');
				$dataqty = Keranjang::where('user_id', Auth::user()->id)->sum('qty');
				$databrt = Keranjang::where('user_id', Auth::user()->id)->sum('total_berat');
			}
			$request->session()->put('dataSum', $dataSum);
			$request->session()->put('dataqty', $dataqty);
			$request->session()->put('databerat', $databrt);
			
			// dd($databrt);
			return view('web.cart.index')->with(compact('dataKeranjang','countKeranjang','dataSum'));

		}else{

			return view('web.errors.404');

		}
	}

	public function savecart(Request $request)
	{
		$messages = [
            // 'required' => 'Field :attribute wajib',
        ];

        $this->validate($request,[
			'ukuran' => 'required|not_in:0',
			'jumlah_qty' => 'required',
        ], $messages);

		$id = $request->id_barang;
		$dataBarang = Product::where('id', $id)->first();

		$cekDataKeranjang = Keranjang::where('barang_id', $request->id_barang)->where('ukuran', $request->ukuran)->where('user_id', Auth::user()->id)->first();

		$berat = $dataBarang->berat;
		$qty = $request->jumlah_qty;
		$totalBerat = $berat * $qty;
		// dd($totalBerat);
		if(!$cekDataKeranjang){

			$data = new Keranjang;
			$data->barang_id = $dataBarang->id;
			$data->user_id = Auth::user()->id;
			$data->qty = $qty;
			$data->harga = $dataBarang->harga;
			$data->total = $data->qty*$data->harga;
			$data->ukuran = $request->ukuran;
			$data->total_berat = $totalBerat;
			$data->save();
			// dd('save baru');

		}else{

			$data = Keranjang::where('barang_id', $request->id_barang)->where('ukuran', $request->ukuran)->where('user_id', Auth::user()->id)->first();
			// dd($data);
			$data->barang_id = $dataBarang->id;
			$data->qty = $data->qty+$request->jumlah_qty;
			$data->total = $data->qty*$data->harga;
			$data->save();

			// dd('ubah qty');

		}

		return redirect('cart')->with('flash_message','Data Berhasil Ditambah');

	}

	public function editcart(Request $request, $id)
	{
		// finish
		$data = Keranjang::where('id', $id)->first();
		$dataProduk = Product::where('id', $data->barang_id)->first();

		$data->qty = $request->updateqty;
		$data->total_berat = $request->updateqty * $dataProduk->berat;
		$data->total = $data->qty*$data->harga;
		// dd($dataProduk);
		$data->save();

		return redirect('cart')->with('flash_message','Data Berhasil Diubah');
	}

	public function deletecart($id)
	{
        // hapus data
        Keranjang::where('id',$id)->delete();
    
        return redirect('cart')->with('flash_message','Data Berhasil Didelete');
	}
	
	public function historypembelian()
	{

		if ( Auth::check() ) {
			$dataTransaksi = Transaksi::where('user_id', Auth::user()->id)->get();

			if(!$dataTransaksi){
				$countTransaksi = 0;
			}else{
				$countTransaksi = $dataTransaksi->count();
			}
	
			return view('web.historypembelian.index')->with(compact('dataTransaksi','countTransaksi'));

		}else{

			return view('web.errors.404');

		}

	}

	public function viewhistory($id)
	{
		if ( Auth::check() ) {
			$dataOrder = Order::where('transaksi_id', $id)->get();
			$dataPengiriman = Pengiriman::where('id_transaksi', $id)->first();
			// dd($dataPengiriman);
				if(!$dataOrder){
					$countOrder = 0;
				}else{
					$countOrder = $dataOrder->count();
					$dataSum = Order::where('transaksi_id', $id)->sum('total');
				}
			// dd($dataPengiriman->resi_pengiriman);
			return view('web.historypembelian.view')->with(compact('dataOrder','countOrder', 'dataPengiriman', 'dataSum'));
		}else{
			return view('web.errors.404');

		}
	}
		
	public function invoice(Request $request)
	{
		$dataSession = $request->session()->all();
		$dataSum = $dataSession['dataSum'];
		// dd($dataSession);
		$cekTransaksi =  Transaksi::latest()->first();
		if(!$cekTransaksi){
			$kodeBelakang = 1;
		}else{
			$kodeBelakang = $cekTransaksi->id+1;
		}
		$kodeDepan = 'KDTR'.$kodeBelakang ;
		$request->session()->put('kodeDepan', $kodeDepan);
		$data = Keranjang::where('user_id', Auth::user()->id)->get();

		$couriers = Courier::pluck('title', 'code');
		$provinces = Province::pluck('title', 'province_id');
		// $city = City::pluck('title', 'id');
		// dd($data);

		// dd($dataSum);

		return view('web.invoice.index')->with(compact('kodeDepan','data','dataSum', 'couriers', 'provinces'));
	}

	public function getCities($id)
	{
		$city = City::where('province_id', $id)->pluck('title', 'city_id');
		// dd($city);
		return json_encode($city);
	}

	public function submit(Request $request)
	{	
		$dataSession = $request->session()->all();
		$dataSum = $dataSession['dataSum'];
		$kodeDepan = $dataSession['kodeDepan'];
		// $qty = $dataSession['dataqty'];
		$berat= $dataSession['databerat'];
		
		// dd($request);
		// $city_origin,$city_destination,$weight,$courier
		$request->session()->put('Provinsi',$request->province_destination);
		// dd($dataSession);
		// dd($request->province_destination);
		$kota_asal=$request->city_origin;
		$kota_tujuan=$request->city_destination;
		$kurir=$request->courier;
		// $courier="tiki";
		// dd($dataSum);
		$cost = RajaOngkir::ongkosKirim([
			'origin' => $request->city_origin,
			'destination' => $request->city_destination,
			'weight' => $berat,
			'courier' => $request->courier,
		])->get();

		if ($request->courier=="jne") {
			$tarif =($cost[0]["costs"][1]["cost"][0]["value"]);//jne reg
			$durasi = ($cost[0]["costs"][1]["cost"][0]["etd"]);
		} elseif($request->courier=="pos"){
			//pos
			$tarif = ($cost[0]["costs"][0]["cost"][0]["value"]);//harga
			$durasi = ($cost[0]["costs"][0]["cost"][0]["etd"]); // waktu
		}else {
			$tarif = ($cost[0]["costs"][0]["cost"][0]["value"]);
			$durasi = ($cost[0]["costs"][0]["cost"][0]["etd"]);
		}
		
		$kota_origin = City::where('city_id', $kota_asal)->first();
		$kota_destination = City::where('city_id', $kota_tujuan)->first();
		$provinsi = Province::where('id', $request->province_destination)->first();
		// dd($provinsi);
		// $kotaAsal = $kota_origin->title;
		// dd($kota_destination->title);
		// dd($kota_origin->title);

		// dd($cost);


		if ( Auth::check() ) {
			$dataKeranjang = Keranjang::where('user_id', Auth::user()->id)->get();
			if(!$dataKeranjang){
				$countKeranjang = 0;
				$dataSum = 0;
			}else{
				$countKeranjang = $dataKeranjang->count();
				$dataSum = Keranjang::where('user_id', Auth::user()->id)->sum('total');
				$dataqty = Keranjang::where('user_id', Auth::user()->id)->sum('qty');
				$databrt = Keranjang::where('user_id', Auth::user()->id)->sum('total_berat');
			}
			$alamat = $request->alamat;
			$total_final = $dataSum + $tarif;
			$kurir = $request->courier;

			$request->session()->put('dataSum', $dataSum);
			$request->session()->put('dataqty', $dataqty);
			$request->session()->put('databerat', $databrt);
			$request->session()->put('alamat', $alamat);
			$request->session()->put('totalfinal', $total_final);
			// tbpengiriman
			$request->session()->put('kotaAsal', $kota_origin->title);
			$request->session()->put('kotaTujuan', $kota_destination->title);
			$request->session()->put('provinsi', $provinsi->title);
			$request->session()->put('kurir', $kurir);
			$request->session()->put('tarif', $tarif);
			$request->session()->put('estimasi', $durasi);
			$request->session()->put('totalberat', $berat);
		}
		return view('web.invoice.invoice')->with(compact('dataSession', 'tarif', 'durasi','kota_origin','kota_destination','kurir', 'countKeranjang', 'dataKeranjang', 'dataSum', 'alamat', 'total_final'));
	}
		
	public function payment(Request $request)
    {
		$dataSession = $request->session()->all();
		$kodeDepan = $dataSession['kodeDepan'];

		$dataBank = Bank::all();
		
		return view('web.payment.index')->with(compact('kodeDepan','dataBank'));
	}

	public function savepayment(Request $request)
    {
		// save item
		$dataSession = $request->session()->all();
		$dataSum = $dataSession['totalfinal'];
		$alamat = $dataSession['alamat'];
		$hapusString = substr($dataSum, 0);
		// dd($dataSession);

		$messages = [
			'not_in'    => 'Pilih terlebih dahulu'
        ];

        $this->validate($request,[
            'data_bank' => 'required|not_in:0',
		], $messages);

		$data = new Transaksi;
		$data->user_id = Auth::user()->id;
		$data->total = $hapusString;
		$data->nama_bank = $request->data_bank;
		$data->status_pembayaran = 1;
		$data->alamat = $alamat;
		$data->created_at = Date("Y-m-d H:i:s", time()+60*60*7);
		$data->updated_at = null;

		if ($request->hasFile('bukti_foto')) {
                // menyimpan data file yang diupload ke variabel $file
                $file = $request->file('bukti_foto');
				$nama_file = $file->getClientOriginalName();
				
				$tujuan_upload = 'template/web/img/bukti_pembayaran/';
				
                $path = $file->move($tujuan_upload,$nama_file);
				
				$data->bukti_foto = $nama_file;
		}

		// dd($data);
		$data->save();

		// tbpengiriman
		$dataPengiriman = new pengiriman;
		$dataPengiriman->id_transaksi = $data->id;
		$dataPengiriman->kota_asal = $dataSession['kotaAsal'];
		$dataPengiriman->provinsi_tujuan = $dataSession['provinsi'];
		// dd($dataPengiriman->provinsi_tujuan);
		$dataPengiriman->kota_tujuan = $dataSession['kotaTujuan'];
		$dataPengiriman->nama_kurir = $dataSession['kurir'];
		$dataPengiriman->total_berat = $dataSession['totalberat'];
		$dataPengiriman->ongkir = $dataSession['tarif'];
		$dataPengiriman->estimasi = $dataSession['estimasi'];
		// dd($dataPengiriman);
		$dataPengiriman->save();
		
		// tbproduk
		$order_items = Keranjang::where('user_id', '=', Auth::user()->id )->get()->toArray();
        foreach ($order_items as $k => $v) 
        {
			$dataOrder[$k] = new Order;
			$dataOrder[$k]->transaksi_id = $data->id;
			$dataOrder[$k]->barang_id = $v['barang_id'];
			$dataOrder[$k]->qty = $v['qty'];
			$dataOrder[$k]->harga = $v['harga'];
			$dataOrder[$k]->total = $v['total'];
			$dataOrder[$k]->created_at = $v['created_at'];
			$dataOrder[$k]->updated_at = $v['updated_at'];
			$dataOrder[$k]->ukuran = $v['ukuran'];
			$dataOrder[$k]->total_berat = $v['total_berat'];
			$dataOrder[$k]->save();
		}
		
		Keranjang::where('user_id', Auth::user()->id)->delete();

		return redirect('historypembelian')->with('flash_message','Tunggu Konfirmasi Admin');

	}
	
	public function login()
    {
		return view('web.login.index');
	}

	public function postlogin(Request $request)
    {
		$email = $request->email;
		$password = $request->password;
        Auth::attempt(['email' => $email, 'password' => $password]);
        if ( Auth::check() ) {
			if(Auth::user()->modul_name == 0){
				return redirect('/')->with('flash_message','Login Berhasil');
			}else{
				Auth::logout();
				return redirect('login')->with('flash_message_error','Email atau Password salah');
			}
        }else {
			return redirect('login')->with('flash_message_error','Email atau Password salah');
        }
	}

	public function logout()
    {
		Auth::logout();
        return redirect('/')->with('flash_message','Logout');
	}

	public function registrasi()
    {
		return view('web.registrasi.index');
	}

	public function postregistrasi(Request $request)
    {
		$messages = [
			'required' => 'Field Wajib diisi!!!',
            'unique'    => 'Email sudah digunakan',
		];

		$this->validate($request,[
			'name' => 'required',
			'email' => 'required|unique:users,email',
			'no_hp' => 'required|unique:users,no_hp',
			'password' => 'required',
			'alamat' => 'required',
		], $messages);

		$data =  new User();
		$data->modul_name = 0;
		$data->name = $request->name;
		$data->email = $request->email;
		$data->no_hp = $request->no_hp;
		$data->password = bcrypt($request->password);
		$data->alamat = $request->alamat;
		$data->created_at = Date("Y-m-d H:i:s", time()+60*60*7);
		$data->updated_at = null;
		$data->save();

		return redirect('login')->with('flash_message','Registrasi Berhasil, Silahkan Login !');
	}
}
